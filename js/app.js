
(function() {

  var calendarMonths = [
    'January'  , 'February', 'March'   , 'April',
    'May'      , 'June'    , 'July'    , 'August',
    'September', 'October' , 'November', 'December'
  ];

  var calendarWeeks = [
    'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
  ];

  window.XTemplate = (function() {

    function camelCase(input) {
      return input.toLowerCase().replace(/-(.)/g, function (match, group1) {
        return group1.toUpperCase();
      });
    }

    var XTemplateObject = {
      init: function() {
        $('script[type="text/x-calendar-template"][id]').each(function() {
          var id = $(this).attr('id');
          var content = $('#'+id).text();
          XTemplateObject[camelCase(id)] = $(content).clone();
          $('#'+id).remove();
        });
      },
      get: function(id) {
        var template = XTemplateObject[camelCase(id)] || XTemplateObject[id] || null;
        return (template && template.clone()) || null;
      }
    };

    return XTemplateObject;

  })();

  window.Meetings = (function() {

    function getFormattedDate(date) {
      return date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate(); 
    }

    function getFormattedTime(date) {
      return date.getHours() + ":" + ('0' + date.getMinutes()).slice(-2);
    }

    var meetingsObject = {
      get: function(date) {
        var meetings = meetingsObject[getFormattedDate(date)] || null;
        return meetings && meetings.length? meetings: []; 
      },
      set: function(meeting) {
        if (meeting && meeting.date) {
          var dateKey = getFormattedDate(meeting.date);
          if (!meetingsObject[dateKey]) {
            meetingsObject[dateKey] = [];
          }
          meetingsObject[dateKey].push({
            date: meeting.date,
            time: getFormattedTime(meeting.date),
            title: meeting.title || "",
            description: meeting.description || ""
          });
        }
      }
    };

    return meetingsObject;

  })();

  function setDummyAppointments() {
    Meetings.set({
      date: (new Date(2016, new Date().getMonth(), 1, 15, 30)),
      title: "Appointment 1",
      description: "This is an appointment"
    });

    Meetings.set({
      date: (new Date(2016, new Date().getMonth(), 8, 17, 0)),
      title: "Appointment 2",
      description: "This is an important meeting"
    });

    Meetings.set({
      date: (new Date(2016, new Date().getMonth(), 1, 10, 30)),
      title: "Appointment 3",
      description: "This is another appointment"
    });

    Meetings.set({
      date: (new Date(2016, new Date().getMonth(), 24, 19, 15)),
      title: "Appointment 345",
      description: "This is a long appointment"
    });
  }

  function init() {
    XTemplate.init();
    setDummyAppointments();
    renderMonthView(new Date());
  }

  function renderMonthView(date) {
    
    var monthView = $('#my-meeting-calendar .month-view');
    var monthViewBody = monthView.find('.month-view-body');
    var monthViewWeekStrip = monthView.find('.month-view-week-strip');

    initHead(monthView, calendarMonths[date.getMonth()], {
      left: function(e) {
        renderMonthView(new Date(date.getFullYear(), date.getMonth() - 1));
      },
      right: function(e) {
        renderMonthView(new Date(date.getFullYear(), date.getMonth() + 1));
      }
    });

    setTimeout(renderMonthViewWeekStrip, 0, monthViewWeekStrip);
    setTimeout(renderMonthViewDays, 0, monthViewBody, date);
  }

  function renderMonthViewWeekStrip(weekStrip) {

    // Clearing existing week strip content
    weekStrip.empty();

    for (var index = 0; index < calendarWeeks.length; index++) {
      var weekLabel = XTemplate.get('month-view-week-strip-label');
      weekLabel.text(calendarWeeks[index]);
      weekStrip.append(weekLabel);
    }
  }

  function renderMonthViewDays(monthViewBody, date) {
    
    // Clearing existing month body content
    monthViewBody.empty();

    var index,
      numberOfDaysInMonth = new Date(date.getFullYear(), date.getMonth()+1, 0).getDate(),
      numberOfDaysInPreviousMonth = new Date(date.getFullYear(), date.getMonth(), 0).getDate(),
      startDayIndex = new Date(date.getFullYear(), date.getMonth(), 1).getDay(),
      endCount = 42 - numberOfDaysInMonth - startDayIndex;
    
    for (index = startDayIndex - 1; index >= 0; index--) {
      
      var day = XTemplate.get('month-view-day-grayed-out');
      var dayTitle = day.find('.day-title');
      var dayBody = day.find('.day-body');
      var appointments = Meetings.get(new Date(date.getFullYear(), date.getMonth()-1, numberOfDaysInPreviousMonth - index));
    
      dayTitle.text(numberOfDaysInPreviousMonth - index);
      setTimeout(renderAppointments, 0, dayBody, appointments);
      monthViewBody.append(day);
    }

    for (index = 0; index < numberOfDaysInMonth; index++) {
      
      var day = XTemplate.get('month-view-day');
      var dayTitle = day.find('.day-title');
      var dayBody = day.find('.day-body');
      var appointments = Meetings.get(new Date(date.getFullYear(), date.getMonth(), index+1));
    
      dayTitle.text(index+1);
      setTimeout(renderAppointments, 0, dayBody, appointments);
      monthViewBody.append(day);
    }

    for (index = 0; index < endCount; index++) {
      
      var day = XTemplate.get('month-view-day-grayed-out');
      var dayTitle = day.find('.day-title');
      var dayBody = day.find('.day-body');
      var appointments = Meetings.get(new Date(date.getFullYear(), date.getMonth()+1, index+1));
    
      dayTitle.text(index+1);
      setTimeout(renderAppointments, 0, dayBody, appointments);
      monthViewBody.append(day);
    }
  }

  function renderAppointments(dayBody, appointments) {

    for (var index = 0; index < appointments.length; index++) {

      var appointmentView = XTemplate.get('month-view-day-appointment');
      appointmentView.attr('title', appointments[index].title);
      appointmentView.find('.appointment-time').text(appointments[index].time);
      appointmentView.find('.appointment-title').text(appointments[index].title);

      dayBody.append(appointmentView);
    }
  }

  function initHead(view, title, listeners) {
    if (view) {
        title && view.find('.view-title').text(title);
        listeners && listeners.left && view.find('.icon-left').off().on('click', listeners.left);
        listeners && listeners.right && view.find('.icon-right').off().on('click', listeners.right);
    }
  }

  $(document).ready(init);  

})();